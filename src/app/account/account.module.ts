import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AccountRouterModule } from './account.route';
import { AccountAppComponent } from './account.app.component';
import { CustomFormsModule } from 'ngx-custom-validators';
import { AccountService } from './services/account.services';
import { RegisterGuard } from './services/account.guard';

@NgModule({
  declarations: [AccountAppComponent, RegisterComponent, LoginComponent],
  imports: [
    CommonModule,
    RouterModule,
    AccountRouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CustomFormsModule,
  ],
  providers: [AccountService, RegisterGuard],
})
export class AccountModule {}
