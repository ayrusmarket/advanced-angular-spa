import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountAppComponent } from './account.app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterGuard } from './services/account.guard';

const accountRouterConfig: Routes = [
  {
    path: '',
    component: AccountAppComponent,
    children: [
      {
        path: 'cadastro',
        component: RegisterComponent,
        canActivate: [RegisterGuard],
        canDeactivate: [RegisterGuard],
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [RegisterGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(accountRouterConfig)],
  exports: [RouterModule],
})
export class AccountRouterModule {}
