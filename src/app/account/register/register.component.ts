import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChildren,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidators } from 'ngx-custom-validators';
import { ToastrService } from 'ngx-toastr';
import { FormBaseComponent } from 'src/app/base-components/form-base.component';
import { User } from '../models/user';
import { AccountService } from '../services/account.services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent
  extends FormBaseComponent
  implements OnInit, AfterViewInit
{
  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements!: ElementRef[];
  errors: any[] = [];
  registerForm!: FormGroup;
  user!: User;
  changesNotSaved!: boolean;

  constructor(
    private fb: FormBuilder,
    private accounService: AccountService,
    private router: Router,
    private toastr: ToastrService
  ) {
    super();
    this.validationMessages = {
      email: {
        required: 'Informe o e-mail',
        email: 'Email inválido',
      },
      password: {
        required: 'Informe a senha',
        rangeLength: 'A senha deve possuir entre 6 e 15 caracteres',
      },
      confirmPassword: {
        required: 'Informe a senha novamente',
        rangeLength: 'A senha deve possuir entre 6 e 15 caracteres',
        equalTo: 'As senhas não conferem',
      },
    };
    super.configureMessagesValidationBase(this.validationMessages);
  }

  ngOnInit(): void {
    let password = new FormControl('', [
      Validators.required,
      CustomValidators.rangeLength([6, 15]),
    ]);
    let confirmPassword = new FormControl('', [
      Validators.required,
      CustomValidators.rangeLength([6, 15]),
      CustomValidators.equalTo(password),
    ]);

    this.registerForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: password,
      confirmPassword: confirmPassword,
    });
  }

  ngAfterViewInit(): void {
    super.configureValidationFormBase(
      this.formInputElements,
      this.registerForm
    );
  }

  addAccount() {
    if (this.registerForm.dirty && this.registerForm.valid) {
      this.user = Object.assign({}, this.user, this.registerForm?.value);
      this.accounService.registerUser(this.user).subscribe({
        next: (success) => this.processSuccess(success),
        error: (error) => this.processError(error),
      });
      this.changesNotSaved = false;
    }
  }

  processSuccess(response: any) {
    this.registerForm.reset();
    this.errors = [];
    this.accounService.localStorage.saveUserLocalData(response);
    let toast = this.toastr.success(
      'Registro realizado com sucesso!',
      'Sucesso'
    );
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/home']);
      });
    }
  }

  processError(response: any) {
    this.errors = response.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Atenção');
  }
}
