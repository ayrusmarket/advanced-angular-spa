import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageUtils } from 'src/app/utils/localstorage';

@Component({
  selector: 'app-menu-login',
  templateUrl: './menu-login.component.html',
})
export class MenuLoginComponent {
  token: any = '';
  user: any;
  email: any = '';
  localStorageUtils = new LocalStorageUtils();

  constructor(private router: Router) {}
  loggedInUser(): boolean {
    if (this.localStorageUtils.getTokenUser()) {
      this.token = this.localStorageUtils.getTokenUser()?.toString();
    } else {
      this.token = null;
    }
    if (this.localStorageUtils.getUser()) {
      this.user = JSON.parse(this.localStorageUtils.getUser() || '{}');
    }
    if (this.user) this.email = this.user.email;

    return this.token !== null;
  }

  logout() {
    this.localStorageUtils.clearUserLocalData();
    this.router.navigate(['/home']);
  }
}
