import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
})
export class DeleteComponent {
  product: Product;
  imagens: string = environment.imagensUrl;
  constructor(
    private produtoService: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.product = this.route.snapshot.data['produto'];
  }

  public excluirProduto() {
    this.produtoService.deleteProduct(this.product.id).subscribe(
      (evento) => {
        this.sucessDelete(evento);
      },
      () => {
        this.fail();
      }
    );
  }

  public sucessDelete(evento: any) {
    const toast = this.toastr.success(
      'Produto excluido com Sucesso!',
      'Good bye :D'
    );
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/produtos/listar-todos']);
      });
    }
  }

  public fail() {
    this.toastr.error('Houve um erro no processamento!', 'Ops! :(');
  }
}
