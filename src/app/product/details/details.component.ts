import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
})
export class DetailsComponent {
  imagens: string = environment.imagensUrl;
  product: Product;

  constructor(private route: ActivatedRoute) {
    this.product = this.route.snapshot.data['produto'];
  }
}
