import { Component, OnInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormControlName } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { CurrencyUtils } from 'src/app/utils/currency-utils';
import { ProductService } from '../services/product.service';
import { ProdutoBaseComponent } from '../product-form.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
})
export class EditComponent extends ProdutoBaseComponent implements OnInit {
  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements!: ElementRef[];
  imagens: string = environment.imagensUrl;
  imageBase64: any;
  imagemPreview: any;
  imagemNome!: string;
  imagemOriginalSrc!: string;

  constructor(
    private fb: FormBuilder,
    private produtoService: ProductService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    super();
    this.product = this.route.snapshot.data['produto'];
  }

  ngOnInit(): void {
    this.produtoService
      .getProviders()
      .subscribe((providers) => (this.providers = providers));

    this.productForm = this.fb.group({
      fornecedorId: ['', [Validators.required]],
      nome: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(200),
        ],
      ],
      descricao: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(1000),
        ],
      ],
      imagem: [''],
      valor: ['', [Validators.required]],
      ativo: [0],
    });

    this.productForm.patchValue({
      fornecedorId: this.product.fornecedorId,
      id: this.product.id,
      nome: this.product.nome,
      descricao: this.product.descricao,
      ativo: this.product.ativo,
      valor: this.product.valor,
    });
    // utilizar o [src] na imagem para evitar que se perca após post
    this.imagemOriginalSrc = this.imagens + this.product.imagem;
  }

  ngAfterViewInit(): void {
    super.configureValidationForm(this.formInputElements);
  }

  editProduct() {
    if (this.productForm.dirty && this.productForm.valid) {
      this.product = Object.assign({}, this.product, this.productForm.value);

      if (this.imageBase64) {
        this.product.imagemUpload = this.imageBase64;
        this.product.imagem = this.imagemNome;
      }

      this.product.valor = CurrencyUtils.StringParaDecimal(this.product.valor);

      this.produtoService.updateProduct(this.product).subscribe(
        (sucesso) => {
          this.processSuccess(sucesso);
        },
        (falha) => {
          this.processFail(falha);
        }
      );

      this.unsavedChanges = false;
    }
  }

  processSuccess(response: any) {
    this.productForm.reset();
    this.errors = [];

    let toast = this.toastr.success('Produto editado com sucesso!', 'Sucesso!');
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/produtos/listar-todos']);
      });
    }
  }

  processFail(fail: any) {
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
  }

  upload(file: any) {
    this.imagemNome = file[0].name;

    var reader = new FileReader();
    reader.onload = this.manipulateReader.bind(this);
    reader.readAsBinaryString(file[0]);
  }

  manipulateReader(readerEvt: any) {
    var binaryString = readerEvt.target.result;
    this.imageBase64 = btoa(binaryString);
    this.imagemPreview = 'data:image/jpeg;base64,' + this.imageBase64;
  }
}
