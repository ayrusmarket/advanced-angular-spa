import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
})
export class ListComponent implements OnInit {
  imagens: string = environment.imagensUrl;
  public products!: Product[];
  errorMessage!: string;

  constructor(private produtoService: ProductService) {}

  ngOnInit(): void {
    this.produtoService.getAll().subscribe(
      (products) => (this.products = products),
      (error) => this.errorMessage
    );
  }
}
