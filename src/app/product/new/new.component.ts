import { Component, OnInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormControlName } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {
  ImageCroppedEvent,
  ImageTransform,
  Dimensions,
} from 'ngx-image-cropper';
import { ProductService } from '../services/product.service';
import { CurrencyUtils } from 'src/app/utils/currency-utils';
import { ProdutoBaseComponent } from '../product-form.base.component';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
})
export class NewComponent extends ProdutoBaseComponent implements OnInit {
  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements!: ElementRef[];

  imageChangedEvent: any = '';
  croppedImage: any = '';
  canvasRotation: any = 0;
  rotation = 0;
  scale = 1;
  showCropper = false;
  containWithinAspectRatio = false;
  transform: ImageTransform = {};
  imageURL!: string;
  imagemNome!: string;

  formResult: string = '';

  constructor(
    private fb: FormBuilder,
    private produtoService: ProductService,
    private router: Router,
    private toastr: ToastrService
  ) {
    super();
  }

  ngOnInit(): void {
    this.produtoService
      .getProviders()
      .subscribe((providers) => (this.providers = providers));

    this.productForm = this.fb.group({
      fornecedorId: ['', [Validators.required]],
      nome: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(200),
        ],
      ],
      descricao: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(1000),
        ],
      ],
      imagem: ['', [Validators.required]],
      valor: ['', [Validators.required]],
      ativo: [true],
    });
  }

  ngAfterViewInit(): void {
    super.configureValidationForm(this.formInputElements);
  }

  addProduct() {
    if (this.productForm.dirty && this.productForm.valid) {
      this.product = Object.assign({}, this.product, this.productForm.value);

      this.product.imagemUpload = this.croppedImage.split(',')[1];
      this.product.imagem = this.imagemNome;
      this.product.valor = CurrencyUtils.StringParaDecimal(this.product.valor);

      this.produtoService.newProduct(this.product).subscribe(
        (sucesso) => {
          this.processSuccess(sucesso);
        },
        (falha) => {
          this.processFail(falha);
        }
      );

      this.unsavedChanges = false;
    }
  }

  processSuccess(response: any) {
    this.productForm.reset();
    this.errors = [];

    let toast = this.toastr.success(
      'Produto cadastrado com sucesso!',
      'Sucesso!'
    );
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/produtos/listar-todos']);
      });
    }
  }

  processFail(fail: any) {
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.imagemNome = event.currentTarget.files[0].name;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    this.showCropper = true;
  }
  cropperReady(sourceImageDimensions: Dimensions) {
    console.log('Cropper ready', sourceImageDimensions);
  }
  loadImageFailed() {
    this.errors.push(
      'O formato do arquivo ' + this.imagemNome + ' não é aceito.'
    );
  }
}
