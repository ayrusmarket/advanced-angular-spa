import { Product, Provider } from './models/product';
import { FormGroup } from '@angular/forms';
import { ElementRef } from '@angular/core';

import { utilsBr } from 'js-brasil';

import { FormBaseComponent } from '../base-components/form-base.component';

export abstract class ProdutoBaseComponent extends FormBaseComponent {
  product!: Product;
  providers!: Provider[];
  errors: any[] = [];
  productForm!: FormGroup;

  MASKS = utilsBr.MASKS;

  constructor() {
    super();

    this.validationMessages = {
      fornecedorId: {
        required: 'Escolha um fornecedor',
      },
      nome: {
        required: 'Informe o Nome',
        minlength: 'Mínimo de 2 caracteres',
        maxlength: 'Máximo de 200 caracteres',
      },
      descricao: {
        required: 'Informe a Descrição',
        minlength: 'Mínimo de 2 caracteres',
        maxlength: 'Máximo de 1000 caracteres',
      },
      imagem: {
        required: 'Informe a Imagem',
      },
      valor: {
        required: 'Informe o Valor',
      },
    };

    super.configureMessagesValidationBase(this.validationMessages);
  }

  protected configureValidationForm(formInputElements: ElementRef[]) {
    super.configureValidationFormBase(formInputElements, this.productForm);
  }
}
