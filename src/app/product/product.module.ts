import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { DetailsComponent } from './details/details.component';
import { DeleteComponent } from './delete/delete.component';
import { NgBrazil } from 'ng-brazil';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductRoutingModule } from './product.route';
import { ProductService } from './services/product.service';
import { ProductGuard } from './services/product.guard';
import { ProductResolve } from './services/product.resolve';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [
    ProductComponent,
    NewComponent,
    ListComponent,
    EditComponent,
    DetailsComponent,
    DeleteComponent,
  ],
  imports: [
    CommonModule,
    NgBrazil,
    TextMaskModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    ProductRoutingModule,
    ImageCropperModule,
  ],
  providers: [ProductService, ProductGuard, ProductResolve],
})
export class ProductModule {}
