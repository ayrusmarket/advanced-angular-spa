import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductResolve } from './services/product.resolve';
import { ProductGuard } from './services/product.guard';

import { ProductComponent } from './product.component';
import { ListComponent } from './list/list.component';
import { NewComponent } from './new/new.component';
import { EditComponent } from './edit/edit.component';
import { DetailsComponent } from './details/details.component';
import { DeleteComponent } from './delete/delete.component';

const productRouterConfig: Routes = [
  {
    path: '',
    component: ProductComponent,
    children: [
      { path: 'listar-todos', component: ListComponent },
      {
        path: 'adicionar-novo',
        component: NewComponent,
        canDeactivate: [ProductGuard],
        canActivate: [ProductGuard],
        data: [{ claim: { nome: 'Produto', valor: 'Adicionar' } }],
      },
      {
        path: 'editar/:id',
        component: EditComponent,
        canActivate: [ProductGuard],
        data: [{ claim: { nome: 'Produto', valor: 'Atualizar' } }],
        resolve: {
          produto: ProductResolve,
        },
      },
      {
        path: 'detalhes/:id',
        component: DetailsComponent,
        resolve: {
          produto: ProductResolve,
        },
      },
      {
        path: 'excluir/:id',
        component: DeleteComponent,
        canActivate: [ProductGuard],
        data: [{ claim: { nome: 'Produto', valor: 'Excluir' } }],
        resolve: {
          produto: ProductResolve,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(productRouterConfig)],
  exports: [RouterModule],
})
export class ProductRoutingModule {}
