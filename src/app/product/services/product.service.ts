import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { BaseService } from 'src/app/services/base.service';
import { Product, Provider } from '../models/product';

@Injectable()
export class ProductService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  getAll(): Observable<Product[]> {
    return this.http
      .get<Product[]>(this.UrlServiceV1 + 'produtos', super.GetAuthHeaderJson())
      .pipe(catchError(super.serviceError));
  }

  getById(id: string): Observable<Product> {
    return this.http
      .get<Product>(
        this.UrlServiceV1 + 'produtos/' + id,
        super.GetAuthHeaderJson()
      )
      .pipe(catchError(super.serviceError));
  }

  newProduct(product: Product): Observable<Product> {
    return this.http
      .post(this.UrlServiceV1 + 'produtos', product, super.GetAuthHeaderJson())
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  updateProduct(product: Product): Observable<Product> {
    return this.http
      .put(
        this.UrlServiceV1 + 'produtos/' + product.id,
        product,
        super.GetAuthHeaderJson()
      )
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  deleteProduct(id: string): Observable<Product> {
    return this.http
      .delete(this.UrlServiceV1 + 'produtos/' + id, super.GetAuthHeaderJson())
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  getProviders(): Observable<Provider[]> {
    return this.http
      .get<Provider[]>(this.UrlServiceV1 + 'fornecedores')
      .pipe(catchError(super.serviceError));
  }
}
