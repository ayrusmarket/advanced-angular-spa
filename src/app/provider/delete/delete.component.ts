import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Provider } from '../models/provider';
import { ProviderService } from '../services/provider.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
})
export class DeleteComponent {
  provider: Provider = new Provider();
  addressMap;
  errors: any[] = [];

  constructor(
    private providerService: ProviderService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private sanitizer: DomSanitizer
  ) {
    this.provider = this.route.snapshot.data['provider'];
    this.addressMap = this.sanitizer.bypassSecurityTrustResourceUrl(
      environment.googleMapsUrl +
        this.completeAddress() +
        environment.googleMapsAPiKey
    );
  }

  public completeAddress(): string {
    return (
      this.provider.endereco.logradouro +
      ', ' +
      this.provider.endereco.numero +
      ' - ' +
      this.provider.endereco.bairro +
      ', ' +
      this.provider.endereco.cidade +
      ' - ' +
      this.provider.endereco.estado
    );
  }

  deleteEvent() {
    this.providerService.deleteProvider(this.provider.id).subscribe(
      (provider) => {
        this.successDelete(provider);
      },
      (error) => {
        this.fail(error);
      }
    );
  }

  successDelete(event: any) {
    const toast = this.toastr.success(
      'Fornecedor excluido com Sucesso!',
      'Good bye :D'
    );
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/fornecedores/listar-todos']);
      });
    }
  }

  fail(fail: any) {
    this.errors = fail.error.errors;
    this.toastr.error('Houve um erro no processamento!', 'Ops! :(');
  }
}
