import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Provider } from '../models/provider';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
})
export class DetailsComponent {
  provider: Provider = new Provider();
  addressMap;

  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer) {
    this.provider = this.route.snapshot.data['provider'];
    this.addressMap = this.sanitizer.bypassSecurityTrustResourceUrl(
      environment.googleMapsUrl +
        this.completeAddress() +
        environment.googleMapsAPiKey
    );
  }

  public completeAddress(): string {
    return (
      this.provider.endereco.logradouro +
      ', ' +
      this.provider.endereco.numero +
      ' - ' +
      this.provider.endereco.bairro +
      ', ' +
      this.provider.endereco.cidade +
      ' - ' +
      this.provider.endereco.estado
    );
  }
}
