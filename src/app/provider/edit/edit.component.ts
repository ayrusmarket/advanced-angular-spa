import { Component, OnInit, ViewChildren, ElementRef } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControlName,
  AbstractControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable, fromEvent, merge } from 'rxjs';

import { ToastrService } from 'ngx-toastr';

import {
  ValidationMessages,
  GenericValidator,
  DisplayMessage,
} from 'src/app/utils/generic-form-validation';
import { Provider } from '../models/provider';
import { Address, CepConsulta } from '../models/address';
import { ProviderService } from '../services/provider.service';
import { MASKS, NgBrazilValidators } from 'ng-brazil';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StringUtils } from 'src/app/utils/string-utils';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
})
export class EditComponent implements OnInit {
  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements!: ElementRef[];

  errors: any[] = [];
  errorsAddress: any[] = [];
  providerForm!: FormGroup;
  addressForm!: FormGroup;

  provider: Provider = new Provider();
  address: Address = new Address();

  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage = {};
  textDocument: string = '';

  MASKS = MASKS;
  formResult: string = '';
  providerType!: number;
  changesNotSaved!: boolean;

  constructor(
    private fb: FormBuilder,
    private providerService: ProviderService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) {
    this.validationMessages = {
      nome: {
        required: 'Informe o Nome',
      },
      documento: {
        required: 'Informe o Documento',
      },
      logradouro: {
        required: 'Informe o Logradouro',
      },
      numero: {
        required: 'Informe o Número',
      },
      bairro: {
        required: 'Informe o Bairro',
      },
      cep: {
        required: 'Informe o CEP',
      },
      cidade: {
        required: 'Informe a Cidade',
      },
      estado: {
        required: 'Informe o Estado',
      },
    };

    this.genericValidator = new GenericValidator(this.validationMessages);

    this.provider = this.route.snapshot.data['provider'];
    this.providerType = this.provider.tipoFornecedor;
  }

  ngOnInit() {
    this.spinner.show();
    this.providerForm = this.fb.group({
      id: '',
      nome: ['', [Validators.required]],
      documento: '',
      ativo: ['', [Validators.required]],
      tipoFornecedor: ['', [Validators.required]],
    });

    this.addressForm = this.fb.group({
      id: '',
      logradouro: ['', [Validators.required]],
      numero: ['', [Validators.required]],
      complemento: [''],
      bairro: ['', [Validators.required]],
      cep: ['', [Validators.required]],
      cidade: ['', [Validators.required]],
      estado: ['', [Validators.required]],
      fornecedorId: '',
    });
    this.fillForm();
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }

  fillForm() {
    this.providerForm.patchValue({
      id: this.provider.id,
      nome: this.provider.nome,
      ativo: this.provider.ativo,
      tipoFornecedor: this.provider.tipoFornecedor.toString(),
      documento: this.provider.documento,
    });

    if (this.typeSupplierForm().value === '1') {
      this.document().setValidators([
        Validators.required,
        NgBrazilValidators.cpf,
      ]);
    } else {
      this.document().setValidators([
        Validators.required,
        NgBrazilValidators.cnpj,
      ]);
    }

    this.addressForm.patchValue({
      id: this.provider.endereco.id,
      logradouro: this.provider.endereco.logradouro,
      numero: this.provider.endereco.numero,
      complemento: this.provider.endereco.complemento,
      bairro: this.provider.endereco.bairro,
      cep: this.provider.endereco.cep,
      cidade: this.provider.endereco.cidade,
      estado: this.provider.endereco.estado,
    });
  }

  ngAfterViewInit() {
    this.typeSupplierForm().valueChanges.subscribe(() => {
      this.exchangeValidationDocument();
      this.configureElementsValidation();
      this.validateForm();
    });

    this.configureElementsValidation();
  }

  configureElementsValidation() {
    let controlBlurs: Observable<any>[] = this.formInputElements.map(
      (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
    );

    merge(...controlBlurs).subscribe(() => {
      this.validateForm();
    });
  }

  exchangeValidationDocument() {
    if (this.typeSupplierForm().value === '1') {
      this.document().clearValidators();
      this.document().setValidators([
        Validators.required,
        NgBrazilValidators.cpf,
      ]);
    } else {
      this.document().clearValidators();
      this.document().setValidators([
        Validators.required,
        NgBrazilValidators.cnpj,
      ]);
    }
  }

  typeSupplierForm(): AbstractControl {
    return this.providerForm?.get('tipoFornecedor') as FormGroup;
  }

  document(): AbstractControl {
    return this.providerForm?.get('documento') as FormGroup;
  }

  validateForm() {
    this.displayMessage = this.genericValidator.processarMensagens(
      this.providerForm
    );
  }

  zipCodeSearch(zipCode: string) {
    zipCode = StringUtils.justNumbers(zipCode);
    if (zipCode.length < 8) return;

    this.providerService.consultCep(zipCode).subscribe(
      (result) => this.fillInAddressQuery(result),
      (error) => this.errors.push(error)
    );
  }

  fillInAddressQuery(cepConsulta: CepConsulta) {
    this.addressForm.patchValue({
      logradouro: cepConsulta.logradouro,
      bairro: cepConsulta.bairro,
      cep: cepConsulta.cep,
      cidade: cepConsulta.localidade,
      estado: cepConsulta.uf,
    });
  }

  editProvider() {
    if (this.providerForm.dirty && this.providerForm.valid) {
      this.provider = Object.assign({}, this.provider, this.providerForm.value);
      this.provider.documento = StringUtils.justNumbers(
        this.provider.documento
      );

      this.providerService.updateProvider(this.provider).subscribe(
        (success) => {
          this.processSuccess(success);
        },
        (fail) => {
          this.processFail(fail);
        }
      );
    }
  }

  processSuccess(response: any) {
    this.errors = [];

    let toast = this.toastr.success(
      'Fornecedor atualizado com sucesso!',
      'Sucesso!'
    );
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/fornecedores/listar-todos']);
      });
    }
  }

  processFail(fail: any) {
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
  }

  updateAddress() {
    if (this.addressForm.dirty && this.addressForm.valid) {
      this.address = Object.assign({}, this.address, this.addressForm.value);

      this.address.cep = StringUtils.justNumbers(this.address.cep);
      this.address.fornecedorId = this.provider.id;

      this.providerService.updateAddress(this.address).subscribe(
        () => this.processSuccessAddress(this.address),
        (fail) => {
          this.processFailAddress(fail);
        }
      );
    }
  }

  processSuccessAddress(address: Address) {
    this.errors = [];

    this.toastr.success('Endereço atualizado com sucesso!', 'Sucesso!');
    this.provider.endereco = address;
    this.modalService.dismissAll();
  }

  processFailAddress(fail: any) {
    this.errorsAddress = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
  }

  openModal(content: any) {
    this.modalService.open(content);
  }
}
