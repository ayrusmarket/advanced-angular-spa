import { Component, OnInit, ViewChildren, ElementRef } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControlName,
  AbstractControl,
} from '@angular/forms';
import { Router } from '@angular/router';

import { Observable, fromEvent, merge } from 'rxjs';

import { ToastrService } from 'ngx-toastr';

import {
  ValidationMessages,
  GenericValidator,
  DisplayMessage,
} from 'src/app/utils/generic-form-validation';
import { Provider } from '../models/provider';
import { ProviderService } from '../services/provider.service';
import { MASKS, NgBrazilValidators } from 'ng-brazil';
import { StringUtils } from 'src/app/utils/string-utils';
import { CepConsulta } from '../models/address';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
})
export class NewComponent implements OnInit {
  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements!: ElementRef[];

  errors: any[] = [];
  providerForm!: FormGroup;
  provider: Provider = new Provider();

  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage = {};
  textDocument: string = 'CPF (requerido)';

  MASKS = MASKS;
  formResult: string = '';

  unsavedChanges!: boolean;

  constructor(
    private fb: FormBuilder,
    private providerService: ProviderService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.validationMessages = {
      nome: {
        required: 'Informe o Nome',
      },
      documento: {
        required: 'Informe o Documento',
        cpf: 'CPF em formato inválido',
        cnpj: 'CNPJ em formato inválido',
      },
      logradouro: {
        required: 'Informe o Logradouro',
      },
      numero: {
        required: 'Informe o Número',
      },
      bairro: {
        required: 'Informe o Bairro',
      },
      cep: {
        required: 'Informe o CEP',
        cep: 'CEP em formato inválido',
      },
      cidade: {
        required: 'Informe a Cidade',
      },
      estado: {
        required: 'Informe o Estado',
      },
    };

    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngOnInit() {
    this.providerForm = this.fb.group({
      nome: ['', [Validators.required]],
      documento: ['', [Validators.required, NgBrazilValidators.cpf]],
      ativo: ['', [Validators.required]],
      tipoFornecedor: ['', [Validators.required]],
      endereco: this.fb.group({
        logradouro: ['', [Validators.required]],
        numero: ['', [Validators.required]],
        complemento: [''],
        bairro: ['', [Validators.required]],
        cep: ['', [Validators.required, NgBrazilValidators.cep]],
        cidade: ['', [Validators.required]],
        estado: ['', [Validators.required]],
      }),
    });
    this.providerForm.patchValue({ tipoFornecedor: '1', ativo: true });
  }

  ngAfterViewInit(): void {
    this.typeSupplierForm().valueChanges.subscribe(() => {
      this.exchangeValidationDocument();
      this.configureElementsValidation();
      this.validateForm();
    });

    this.configureElementsValidation();
  }

  configureElementsValidation() {
    let controlBlurs: Observable<any>[] = this.formInputElements.map(
      (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur')
    );
    merge(...controlBlurs).subscribe(() => {
      this.validateForm();
    });
  }

  validateForm() {
    this.displayMessage = this.genericValidator.processarMensagens(
      this.providerForm
    );
    this.unsavedChanges = true;
  }

  exchangeValidationDocument() {
    if (this.typeSupplierForm().value === '1') {
      this.document().clearValidators();
      this.document().setValidators([
        Validators.required,
        NgBrazilValidators.cpf,
      ]);
      this.textDocument = 'CPF (requerido)';
    } else {
      this.document().clearValidators();
      this.document().setValidators([
        Validators.required,
        NgBrazilValidators.cnpj,
      ]);
      this.textDocument = 'CNPJ (requerido)';
    }
  }

  typeSupplierForm(): AbstractControl {
    return this.providerForm?.get('tipoFornecedor') as FormGroup;
  }

  document(): AbstractControl {
    return this.providerForm?.get('documento') as FormGroup;
  }

  zipCodeSearch(zipCode: string) {
    zipCode = StringUtils.justNumbers(zipCode);
    if (zipCode.length < 8) return;

    this.providerService.consultCep(zipCode).subscribe(
      (result) => this.fillInAddressQuery(result),
      (error) => this.errors.push(error)
    );
  }

  fillInAddressQuery(zipCodeQuery: CepConsulta) {
    this.providerForm.patchValue({
      endereco: {
        logradouro: zipCodeQuery.logradouro,
        bairro: zipCodeQuery.bairro,
        cep: zipCodeQuery.cep,
        cidade: zipCodeQuery.localidade,
        estado: zipCodeQuery.uf,
      },
    });
  }

  addSupplier() {
    if (this.providerForm.dirty && this.providerForm.valid) {
      this.provider = Object.assign({}, this.provider, this.providerForm.value);
      this.formResult = JSON.stringify(this.provider);

      this.provider.endereco.cep = StringUtils.justNumbers(
        this.provider.endereco.cep
      );
      this.provider.documento = StringUtils.justNumbers(
        this.provider.documento
      );

      this.providerService.newProvider(this.provider).subscribe(
        (success) => {
          this.processSuccess(success);
        },
        (fail) => {
          this.processFail(fail);
        }
      );
    }
  }

  processSuccess(response: any) {
    this.providerForm.reset();
    this.errors = [];
    this.unsavedChanges = false;

    let toast = this.toastr.success(
      'Fornecedor cadastrado com sucesso!',
      'Sucesso!'
    );
    if (toast) {
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/fornecedores/listar-todos']);
      });
    }
  }

  processFail(fail: any) {
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Opa :(');
  }
}
