import { Component, Input } from '@angular/core';
import { Product } from 'src/app/product/models/product';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'products-list',
  templateUrl: './products-list.component.html',
})
export class ProductsListComponent {
  imagens: string = environment.imagensUrl;

  @Input()
  products!: Product[];
}
