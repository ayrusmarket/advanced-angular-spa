import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from './details/details.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { ProviderComponent } from './provider.component';
import { ProviderRoutingModule } from './provider.route';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProviderService } from './services/provider.service';
import { NgBrazil } from 'ng-brazil';
import { TextMaskModule } from 'angular2-text-mask';
import { ProviderResolve } from './services/provider.resolve';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProviderGuard } from './services/provider.guard';
import { ProductsListComponent } from './products-list/products-list.componet';

@NgModule({
  declarations: [
    ProviderComponent,
    DetailsComponent,
    EditComponent,
    DeleteComponent,
    NewComponent,
    ListComponent,
    ProductsListComponent,
  ],
  imports: [
    CommonModule,
    ProviderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgBrazil,
    TextMaskModule,
    NgxSpinnerModule,
  ],
  providers: [ProviderService, ProviderResolve, ProviderGuard],
})
export class ProviderModule {}
