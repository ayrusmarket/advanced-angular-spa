import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeleteComponent } from './delete/delete.component';
import { DetailsComponent } from './details/details.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { NewComponent } from './new/new.component';
import { ProviderComponent } from './provider.component';
import { ProviderGuard } from './services/provider.guard';
import { ProviderResolve } from './services/provider.resolve';

const providerRouterConfig: Routes = [
  {
    path: '',
    component: ProviderComponent,
    children: [
      { path: 'listar-todos', component: ListComponent },
      {
        path: 'adicionar-novo',
        component: NewComponent,
        canDeactivate: [ProviderGuard],
        canActivate: [ProviderGuard],
        data: [{ claim: { nome: 'Fornecedor', valor: 'Adicionar' } }],
      },

      {
        path: 'editar/:id',
        component: EditComponent,
        resolve: {
          provider: ProviderResolve,
        },
      },
      {
        path: 'detalhes/:id',
        component: DetailsComponent,
        resolve: {
          provider: ProviderResolve,
        },
      },
      {
        path: 'excluir/:id',
        component: DeleteComponent,
        canActivate: [ProviderGuard],
        data: [{ claim: { nome: 'Fornecedor', valor: 'Excluir' } }],
        resolve: {
          provider: ProviderResolve,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(providerRouterConfig)],
  exports: [RouterModule],
})
export class ProviderRoutingModule {}
