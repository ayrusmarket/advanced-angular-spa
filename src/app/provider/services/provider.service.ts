import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { BaseService } from 'src/app/services/base.service';
import { Provider } from '../models/provider';
import { Address, CepConsulta } from '../models/address';
@Injectable()
export class ProviderService extends BaseService {
  fornecedor: Provider = new Provider();

  constructor(private http: HttpClient) {
    super();

    this.fornecedor.nome = 'Teste Fake';
    this.fornecedor.documento = '32165498754';
    this.fornecedor.ativo = true;
    this.fornecedor.tipoFornecedor = 1;
  }

  getAll(): Observable<Provider[]> {
    return this.http
      .get<Provider[]>(this.UrlServiceV1 + 'fornecedores')
      .pipe(catchError(super.serviceError));
  }

  getById(id: string): Observable<Provider> {
    return this.http
      .get<Provider>(
        this.UrlServiceV1 + 'fornecedores/' + id,
        super.GetAuthHeaderJson()
      )
      .pipe(catchError(super.serviceError));
  }

  newProvider(provider: Provider): Observable<Provider> {
    provider.tipoFornecedor = provider.tipoFornecedor == 1 ? +'1' : +'2';
    return this.http
      .post(
        this.UrlServiceV1 + 'fornecedores',
        provider,
        this.GetAuthHeaderJson()
      )
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  updateProvider(provider: Provider): Observable<Provider> {
    provider.tipoFornecedor = provider.tipoFornecedor == 1 ? +'1' : +'2';
    return this.http
      .put(
        this.UrlServiceV1 + 'fornecedores/' + provider.id,
        provider,
        super.GetAuthHeaderJson()
      )
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  deleteProvider(id: string): Observable<Provider> {
    return this.http
      .delete(
        this.UrlServiceV1 + 'fornecedores/' + id,
        super.GetAuthHeaderJson()
      )
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  updateAddress(endereco: Address): Observable<Address> {
    return this.http
      .put(
        this.UrlServiceV1 + 'fornecedores/endereco/' + endereco.id,
        endereco,
        super.GetAuthHeaderJson()
      )
      .pipe(map(super.extractData), catchError(super.serviceError));
  }

  consultCep(cep: string): Observable<CepConsulta> {
    return this.http
      .get<CepConsulta>(`https://viacep.com.br/ws/${cep}/json/`)
      .pipe(catchError(super.serviceError));
  }
}
