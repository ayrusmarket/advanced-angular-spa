import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { LocalStorageUtils } from 'src/app/utils/localstorage';

export abstract class BaseGuard {
  private localStorageUtils = new LocalStorageUtils();

  constructor(protected router: Router) {}

  protected validateClaims(routeAc: ActivatedRouteSnapshot): boolean {
    if (!this.localStorageUtils.getTokenUser()) {
      this.router.navigate(['/conta/login/'], {
        queryParams: { returnUrl: this.router.url },
      });
    }

    let user = JSON.parse(this.localStorageUtils.getUser() || '{}');

    let claim: any = routeAc.data[0];
    if (claim !== undefined) {
      let claim = routeAc.data[0]['claim'];

      if (claim) {
        if (!user.claims) {
          this.accessDenied();
        }

        let userClaims = user.claims.find((x: any) => x.type === claim.nome);

        if (!userClaims) {
          this.accessDenied();
        }

        let valoresClaim = userClaims.value as string;

        if (!valoresClaim.includes(claim.valor)) {
          this.accessDenied();
        }
      }
    }

    return true;
  }

  private accessDenied() {
    this.router.navigate(['/acesso-negado']);
  }
}
